from sys import argv
from random import randint

def main():
    if len(argv) < 6:
        print("./create_random_map.py random name width height lower upper")
        print("./create_random_map.py noise name width height times")
        return
    
    name = argv[2]
    width = int(argv[3])
    height = int(argv[4])

    if argv[1] == "random":
        if len(argv) < 7:
            print("./create_random_map.py random name width height lower upper")
            return
        upper = int(argv[6])
        lower = int(argv[5])

        file = open(name, "w")
        for i in range(height):
            file.write(' '.join([str(randint(lower, upper)) for j in range(width)]) + '\n')
        file.close()
    elif argv[1] == "noise":
        try:
            from noise import snoise2
        except ImportError:
            print("Failed to import noise! Unable to do noise things")
            return
        times = int(argv[5])
        file = open(name, "w")
        for i in range(height):
            file.write(' '.join([str(int(snoise2(i, j) * times)) for j in range(width)]) + '\n')
        file.close()

if __name__ == '__main__':
    main()
