/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: astrole <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/16 04:49:02 by astrole           #+#    #+#             */
/*   Updated: 2018/08/16 04:49:02 by astrole          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H
# include "lib.h"

typedef struct	s_grid
{
	t_vector3f	**pos3d;
	t_vector2f	**pos2d;
	double		length_x;
	double		length_y;
	double		max;
	double		min;
	size_t		width;
	size_t		height;
}				t_grid;

typedef struct	s_objects
{
	t_window	window;
	t_image		*image;
	t_grid		*grid;
	t_config	*config;
}				t_objects;

void			terminate(char *reason, int ret);
void			clean(t_window *window, t_image *image,
	t_grid *grid, t_config *config);
t_grid			*parse_file(char *name);
void			draw_points(t_grid *grid, t_window *window, t_image *image,
							t_config *config);
t_grid			*check_for_minmax(t_grid *grid, int i, int j);
#endif
