/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: astrole <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/10 03:58:38 by astrole           #+#    #+#             */
/*   Updated: 2018/08/10 03:58:39 by astrole          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void		set_up_values(t_config *config, t_window *window)
{
	if (config)
	{
		window->size.x = config->width;
		window->size.y = config->height;
	}
	else
	{
		window->size.x = 1000;
		window->size.y = 1000;
	}
}

static void		init(t_grid *grid, t_window *window)
{
	t_vector2f	start_pos;
	size_t		i;
	size_t		j;
	double		hypot;

	hypot = sqrt(pow(grid->width, 2) + pow(grid->height, 2));
	grid->length_x = window->size.x / hypot;
	grid->length_y = window->size.y / hypot;
	start_pos.y = 0;
	i = 0;
	while (i < grid->height)
	{
		j = 0;
		start_pos.x = 0;
		while (j < grid->width)
		{
			grid->pos3d[i][j].x = start_pos.x;
			grid->pos3d[i][j].y = start_pos.y;
			start_pos.x += grid->length_x;
			j++;
		}
		start_pos.y += grid->length_y;
		i++;
	}
}

static void		start(t_window *window, t_image *image,
	t_grid *grid, t_config *config)
{
	if (config)
		fill(window, image, config->background);
	draw_points(grid, window, image, config);
	draw_image(window, image, 0);
	clean(window, image, grid, config);
	mlx_key_hook(window->win_ptr, hook_keydown, window);
	mlx_hook(window->win_ptr, 17, 0, quit_application, window);
	mlx_loop(window->mlx_ptr);
}

int				main(int argc, char **argv)
{
	t_objects objects;

	if (argc != 2)
		terminate("./fdf <FILENAME>", -1);
	objects.config = parse_config("config.txt");
	if (!objects.config)
		ft_putendl("Failed to read config, falling back to normal values!");
	set_up_values(objects.config, &objects.window);
	objects.grid = parse_file(argv[1]);
	if (!objects.grid)
		terminate("Failed to make the grid\n", -3);
	init(objects.grid, &objects.window);
	if (!(objects.window.mlx_ptr = mlx_init()))
		terminate("Failed to connect to mlx", -1);
	if (!(objects.window.win_ptr = mlx_new_window(objects.window.mlx_ptr,
		objects.window.size.x, objects.window.size.y, "FDF")))
		terminate("Failed to create a window!", -2);
	objects.image = new_image(&objects.window,
		(t_vector2f){objects.window.size.x, objects.window.size.y});
	start(&objects.window, objects.image, objects.grid, objects.config);
	return (0);
}
