/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   other.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: astrole <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/10 03:48:11 by astrole           #+#    #+#             */
/*   Updated: 2018/08/10 03:48:12 by astrole          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	terminate(char *reason, int ret)
{
	ft_putstr(reason);
	exit(ret);
}

void	clean(t_window *window, t_image *image, t_grid *grid, t_config *config)
{
	size_t i;

	del_image(window, image);
	i = -1;
	while (++i < grid->height)
	{
		free(grid->pos3d[i]);
		free(grid->pos2d[i]);
	}
	free(grid->pos3d);
	free(grid->pos2d);
	free(grid);
	if (config)
		free(config);
}

t_grid	*check_for_minmax(t_grid *grid, int i, int j)
{
	if (grid->pos3d[i][j].z > grid->max)
		grid->max = grid->pos3d[i][j].z;
	if (grid->pos3d[i][j].z < grid->min)
		grid->min = grid->pos3d[i][j].z;
	return (grid);
}
