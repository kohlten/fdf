/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: astrole <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/02 01:10:41 by astrole           #+#    #+#             */
/*   Updated: 2018/08/02 01:10:43 by astrole          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static t_grid	*get_vectors(t_grid *grid)
{
	size_t i;

	grid->pos3d = ft_memalloc(grid->height * sizeof(t_vector3f*));
	i = -1;
	while (++i < grid->height)
		grid->pos3d[i] = ft_memalloc(sizeof(t_vector3f) * grid->width);
	grid->pos2d = ft_memalloc(grid->height * sizeof(t_vector2f*));
	i = -1;
	while (++i < grid->height)
		grid->pos2d[i] = ft_memalloc(sizeof(t_vector2f) * grid->width);
	return (grid);
}

static char		***get_nums(char *file)
{
	char	***splited;
	void	*old;
	int		i;

	i = 0;
	splited = (char ***)ft_strsplit(file, '\n');
	while (splited[i])
	{
		old = splited[i];
		splited[i] = ft_strsplit((char *)splited[i], ' ');
		free(old);
		i++;
	}
	return (splited);
}

static void		free_nums(char ***splited)
{
	int i;
	int j;

	i = -1;
	while (splited[++i])
	{
		j = -1;
		while (splited[i][++j])
			free(splited[i][j]);
		free(splited[i]);
	}
	free(splited);
}

static t_grid	*parse_string(char *file, t_grid *grid)
{
	char	***splited;
	int		i;
	int		j;

	splited = get_nums(file);
	i = -1;
	while (splited[0][++i])
		grid->width++;
	grid = get_vectors(grid);
	i = -1;
	while (splited[++i])
	{
		j = -1;
		while (splited[i][++j])
		{
			if (!ft_strisnum(splited[i][j]))
				return (NULL);
			grid->pos3d[i][j].z = ft_atoi(splited[i][j]);
			grid = check_for_minmax(grid, i, j);
		}
	}
	free_nums(splited);
	return (grid);
}

/*
** strings[0] = file
** strings[1] = line
** strings[2] = oldFile
** strings[3] = withNewLine
*/

t_grid			*parse_file(char *name)
{
	char	*strings[4];
	t_grid	*out;
	int		ret;
	int		fd;

	fd = open(name, O_RDONLY);
	strings[0] = ft_strnew(0);
	out = ft_memalloc(sizeof(t_grid));
	while ((ret = get_next_line(fd, &strings[1])) > 0)
	{
		strings[2] = strings[0];
		strings[3] = ft_strjoin(strings[1], "\n");
		strings[0] = ft_strjoin(strings[0], strings[3]);
		free(strings[1]);
		free(strings[2]);
		free(strings[3]);
		out->height++;
	}
	close(fd);
	if (ret < 0)
		terminate("Failed to open file!\n", -1);
	out = parse_string(strings[0], out);
	free(strings[0]);
	return (out);
}
