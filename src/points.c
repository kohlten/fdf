/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   points.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: astrole <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/16 05:13:30 by astrole           #+#    #+#             */
/*   Updated: 2018/08/16 05:13:32 by astrole          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static t_grid	*center(t_grid *grid, t_vector2f size)
{
	size_t		i;
	size_t		j;
	t_vector2f	to_move;

	i = -1;
	to_move.x = (size.x / 2) - grid->pos2d[grid->height / 2][grid->width / 2].x;
	to_move.y = (size.y / 2) - grid->pos2d[grid->height / 2][grid->width / 2].y;
	while (++i < grid->height)
	{
		j = -1;
		while (++j < grid->width)
		{
			grid->pos2d[i][j].x += to_move.x;
			grid->pos2d[i][j].y += to_move.y;
		}
	}
	return (grid);
}

static t_grid	*convert_points(t_grid *grid, t_vector2f size, t_config *config)
{
	size_t i;
	size_t j;
	double sensitivity;

	if (!config || config->sensitivity == 0)
		sensitivity = map(grid->max + fabs(grid->min),
			(t_vector2f){0, 20}, (t_vector2f){15, 7});
	else
		sensitivity = config->sensitivity;
	i = -1;
	while (++i < grid->height)
	{
		j = -1;
		while (++j < grid->width)
			grid->pos2d[i][j] = *convert_3d_2d(&grid->pos3d[i][j],
								&grid->pos2d[i][j], sensitivity);
	}
	return (center(grid, size));
}

void			draw_points(t_grid *grid,
							t_window *window,
							t_image *image,
							t_config *config)
{
	size_t	i;
	size_t	j;
	t_line	*line;

	grid = convert_points(grid, window->size, config);
	line = create_line(0, 0);
	i = -1;
	while (++i < grid->height - 1)
	{
		j = -1;
		while (++j < grid->width - 1)
		{
			drawline(window, image, set_line(line, &grid->pos2d[i][j],
&grid->pos2d[i + 1][j], get_color(config, grid->pos3d[i + 1][j].z)));
			drawline(window, image, set_line(line, 0, &grid->pos2d[i][j + 1],
get_color(config, grid->pos3d[i + 1][j].z)));
			if (i > grid->height - 3)
				drawline(window, image, set_line(line, &grid->pos2d[i + 1][j],
&grid->pos2d[i + 1][j + 1], get_color(config, grid->pos3d[i + 1][j].z)));
		}
		drawline(window, image, set_line(line, &grid->pos2d[i][j],
&grid->pos2d[i + 1][j], get_color(config, grid->pos3d[i + 1][j].z)));
	}
	free(line);
}
